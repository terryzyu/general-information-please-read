# Welcome to my Bitbucket! #

I'm Terry Yu, a student at DePaul University studying Computer Science. Here is where I store code and what you see is what I have made publically available. You are free to use the code however you like. My résumé is also here! It's been modified to omit some personal information so if I sent you one it may look slightly different!


Code for classes I am currently enrolled in *will not be uploaded until I have received my final grade for the course.* Additionally, some code or courses will not be uploaded due to one or more of the following reasons.


* I was taught git during my second year, some first year material may not be here.
* The class only required a small amount of code thus I do not believe it's worthwhile to upload.
* It's really bad code that I still need to fix or have abandoned!


### What has been made public (Oct 18, 2018) ###

* General Information (Please Read!)
* tokenBotFinal
* csc299 (Blockchain and Smart Contracts course)
* csc300 (Data Structures I)
* csc301 (Data Structures II)
* Résumé!

# WHAT I AM NOT RESPONSIBLE FOR #

* You using my code and getting caught cheating in class.
	+ You may refer to my code as a guide however I am not endorsing copy pasting directly. My code may even be wrong!
* Ongoing support of my code.


# Thank you for reading and enjoy browsing around! #
